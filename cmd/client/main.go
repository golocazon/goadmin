package main

import (
	"context"
	"github.com/spf13/viper"
	"gitlab.com/golocazon/goadmin/pkg/admin"
	logger "gitlab.com/golocazon/gologger"
	"log"
)

var (
	DefaultLogger logger.Logger
)

func configLogging() {
	config := logger.Configuration{
		EnableConsole:     true,
		ConsoleLevel:      logger.Debug,
		ConsoleJSONFormat: true,
		EnableFile:        true,
		FileLevel:         logger.Info,
		FileJSONFormat:    true,
		FileLocation:      "log.log",
	}

	err := logger.NewLogger(config, logger.InstanceZapLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
	}

	contextLogger := logger.Log.WithFields(logger.Fields{"key1": "value1"})
	contextLogger.Debugf("Starting with zap")
	contextLogger.Infof("Zap is awesome")

	err = logger.NewLogger(config, logger.InstanceLogrusLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
	}
	contextLogger = logger.WithFields(logger.Fields{"key1": "value1"})
	contextLogger.Debugf("Starting with logrus")

	contextLogger.Infof("Logrus is awesome")
	DefaultLogger = logger.WithFields(logger.Fields{"Application": "GoCartClient"})
}

func configConfiguration() {
	viper.SetConfigName("goproduct_config") // name of config file (without extension)
	viper.AddConfigPath("$HOME/.goproduct")      // call multiple times to add many search paths
	viper.AddConfigPath(".")                  // optionally look for config in the working directory

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		//		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func main() {

	configConfiguration()
	configLogging()

	clientService, err :=api.NewClientWithResponses("http://127.0.0.1:1320")

	params := &api.GetUsersParams{

	}
	usersResponse, err := clientService.GetUsersWithResponse(context.Background(), params)
	if err != nil {
		logger.Infof("Error calling GetProducts %v", err)
		panic(err)
	}
	users := usersResponse.JSON200
	if len(*users) == 0 {
		logger.Infof("No user found")
	} else {
		for _, user := range *users {
			logger.Infof("Got user %v", *user.Name)
		}
	}
}
