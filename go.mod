module gitlab.com/golocazon/goadmin

go 1.13

require (
	github.com/deepmap/oapi-codegen v1.3.5
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getkin/kin-openapi v0.2.0
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/go-chi/jwtauth v4.0.4+incompatible
	github.com/go-chi/render v1.0.1
	github.com/jinzhu/gorm v1.9.12
	github.com/labstack/echo/v4 v4.1.14
	github.com/mitchellh/mapstructure v1.1.2
	github.com/spf13/viper v1.6.2
	gitlab.com/golocazon/gologger v0.0.3
	gitlab.com/golocazon/goproduct v0.0.0-20191212225959-6c54b638fda0
	gitlab.com/golocazon/gowsfwk v0.0.1
)
