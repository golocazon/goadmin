package admin

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"gitlab.com/golocazon/goadmin/internal/pkg"
	"gitlab.com/golocazon/goadmin/pkg/models"
	"net/http"
	"strconv"
)

type  DefaultServerInterface struct{
	UserController pkg.UserController
}

func NewAdminServerInterface(pc pkg.UserController) *DefaultServerInterface {
	return &DefaultServerInterface {
		UserController : pc,
	}
}

	// get business details (DELETE /business/{businessId})
func (si * DefaultServerInterface)	DeleteBusiness(w http.ResponseWriter, r *http.Request) {

}
	// get business details (GET /business/{businessId})
func (si * DefaultServerInterface)	GetBusiness(w http.ResponseWriter, r *http.Request) {

}
	// get business details (PATCH /business/{businessId})
func (si * DefaultServerInterface)	 PatchBusiness(w http.ResponseWriter, r *http.Request) {

}
	// get business details (PUT /business/{businessId})
func (si * DefaultServerInterface)		UpdateBusiness(w http.ResponseWriter, r *http.Request) {

}
	// get business users (GET /business/{businessId}/users)
func (si * DefaultServerInterface)		GetBusinessUsers(w http.ResponseWriter, r *http.Request) {

}
	// add a business user (POST /business/{businessId}/users)
func (si * DefaultServerInterface)		CreateBusinessUser(w http.ResponseWriter, r *http.Request) {

}
	// searches businesses (GET /businesses)
func (si * DefaultServerInterface)		GetBusinesses(w http.ResponseWriter, r *http.Request) {

}
	// adds a business (POST /businesses)
func (si * DefaultServerInterface)		AddBusiness(w http.ResponseWriter, r *http.Request) {

}
	// patch a user (DELETE /user/{userId})
func (si * DefaultServerInterface)		DeleteUser(w http.ResponseWriter, r *http.Request) {
	//userID, _ := getUserId(r)

	userIDAsString, ok := r.Context().Value("userId").(string)
	if (!ok) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	userID, err := strconv.ParseUint(userIDAsString, 0, 0)
	if (err != nil) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	err = si.UserController.DeleteUser(uint(userID))
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})

}
	// get a user (GET /user/{userId})
func (si * DefaultServerInterface)		GetUser(w http.ResponseWriter, r *http.Request) {
	//userID, _ := getUserId(r)

	userIDAsString, ok := r.Context().Value("userId").(string)
	if (!ok) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	userID, err := strconv.ParseUint(userIDAsString, 0, 0)
	if (err != nil) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	err, user := si.UserController.GetUser(uint(userID))
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})
	js, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
	// patch a user (PATCH /user/{userId})
func (si * DefaultServerInterface)		PatchUser(w http.ResponseWriter, r *http.Request) {
	userIDAsString, ok := r.Context().Value("userId").(string)
	if (!ok) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	userID, err := strconv.ParseUint(userIDAsString, 0, 0)
	if (err != nil) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	var user models.User
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	err, updatedUser := si.UserController.UpdateUser(uint(userID), user)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})
	js, err := json.Marshal(updatedUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
	// patch a user (PUT /user/{userId})
func (si * DefaultServerInterface)		UpdateUser(w http.ResponseWriter, r *http.Request) {
	//userID, _ := getUserId(r)

	userIDAsString, ok := r.Context().Value("userId").(string)
	if (!ok) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	userID, err := strconv.ParseUint(userIDAsString, 0, 0)
	if (err != nil) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	var user models.User
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	err, updatedUser := si.UserController.UpdateUser(uint(userID), user)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})
	js, err := json.Marshal(updatedUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
	// searches users (GET /users)
func (si * DefaultServerInterface)	GetUsers(w http.ResponseWriter, r *http.Request) {
	//userID, _ := getUserId(r)

	err, users := si.UserController.GetUsers()
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})
	js, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
	// adds a user (POST /users)
func (si * DefaultServerInterface)		AddUser(w http.ResponseWriter, r *http.Request) {
	//userID, _ := getUserId(r)

	var User models.User
	err := json.NewDecoder(r.Body).Decode(&User)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	err, createdUser := si.UserController.CreateUser(User)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	js, err := json.Marshal(createdUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}


func getUserId(r *http.Request) (userID int64, err error) {
	userIDVal := chi.URLParam(r, "userId")
	if len(userIDVal) > 0 {
		userIDAsInt, _ := strconv.Atoi(userIDVal)
		userID = int64(userIDAsInt)
	} else {
		_, claims, _ := jwtauth.FromContext(r.Context())
		userIDAsFloat64 := claims["user_id"].(float64)
		userID = int64(userIDAsFloat64)
	}
	return
}
