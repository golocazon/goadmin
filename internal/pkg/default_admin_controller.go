package pkg

import (
	"errors"
	"gitlab.com/golocazon/goadmin/internal/dbmodels"
	"gitlab.com/golocazon/goadmin/pkg/models"
)

func NewDefaultUserController(UserRepository UserRepository) *DefaultUserController {
	return &DefaultUserController{
		UserRepository: UserRepository,
	}
}

type DefaultUserController struct {
	UserRepository UserRepository
}

func (r *DefaultUserController) CreateUser(User models.User) (error, *models.User) {
	// Create a new User
	dbUser := dbmodels.NewDBUser(&User)
	createdUser, error := r.UserRepository.Store(dbUser)
	return error, dbmodels.NewAPIUser(createdUser)
}


func (r *DefaultUserController) UpdateUser(UserID uint, user models.User) (error, *models.User) {
	err, loadedUser := r.UserRepository.GetByID(UserID)
	if err != nil {
		return errors.New("User does not exist"), nil
	}
	user.ID=loadedUser.ID
	updatedUser := dbmodels.NewDBUser(&user)

	// Create a new User
	updatedUser, error := r.UserRepository.Update(updatedUser)
	return error, dbmodels.NewAPIUser(updatedUser)
}


func (r *DefaultUserController) UpdateUserDetails(UserID uint, User models.User) (error, *models.User) {
	err, loadedUser := r.UserRepository.GetByID(UserID)
	if err != nil {
		return errors.New("User does not exist"), nil
	}
	//TODO: how to ensure the User belongs to the user?? is it needed?
	mergeUserContent(loadedUser, User)
	// Create a new User
	updatedUser, error := r.UserRepository.Update(loadedUser)
	return error, dbmodels.NewAPIUser(updatedUser)
}

/*
Merges
*/
func mergeUserContent(origin *dbmodels.User, delta models.User) {
	if (delta.Name != nil) {

	origin.Name = *delta.Name
	}
}


func (r *DefaultUserController) GetUser(UserID uint) (err error, loadedUser *models.User) {
	err, dbUser :=  r.UserRepository.GetByID(UserID)
	loadedUser = dbmodels.NewAPIUser(dbUser)
	return
}

func (r *DefaultUserController) GetUsers() (err error, loadedUser []*models.User) {
	dbUsers,err :=  r.UserRepository.Fetch(0, 10)
	loadedUser = make ([]*models.User, len(dbUsers))
	for i, _ := range(dbUsers) {
		loadedUser[i] = dbmodels.NewAPIUser(dbUsers[i])
	}
	return
}

func (r *DefaultUserController) DeleteUser(UserID uint) (err error) {
	_, err = r.UserRepository.Delete(UserID)
	return
}
