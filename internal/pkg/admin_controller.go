package pkg

import (
	"gitlab.com/golocazon/goadmin/pkg/models"
)

type UserController interface {
	CreateUser(User models.User) (error, *models.User)

	UpdateUserDetails(UserID uint, User models.User) (error, *models.User)

	UpdateUser(UserID uint, User models.User) (error, *models.User)

	GetUsers() (error, []*models.User)

	GetUser(UserID uint) (error, *models.User)

	DeleteUser(UserID uint) error

}
