package pkg

import (
	"gitlab.com/golocazon/goadmin/internal/dbmodels"
)

type UserRepository interface {
	Fetch(offset int64, num uint) ([]*dbmodels.User, error)
	GetByID(userID uint) (error, *dbmodels.User)
	Update(sser *dbmodels.User) (*dbmodels.User, error)
	Store(a *dbmodels.User) (*dbmodels.User, error)
	Delete(id uint) (bool, error)
}

type BusinessRepository interface {
	Fetch(offset int64, num uint) ([]*dbmodels.Business, error)
	GetByID(businessID uint) (error, *dbmodels.Business)
	Update(business *dbmodels.Business) (*dbmodels.Business, error)
	Store(a *dbmodels.Business) (*dbmodels.Business, error)
	Delete(id uint) (bool, error)
}
