package dbmodels

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/golocazon/goadmin/pkg/models"
)

type User struct {
	gorm.Model
	Name       string    `gorm:"not null";json:"name"`

}

type Business struct {
	gorm.Model
	Name       string    `gorm:"not null";json:"name"`

}

func NewDBUser(user *models.User) *User {
	newUser := User{
		Model: gorm.Model {
			ID:                user.ID,
		},
		Name:              *user.Name,
	}
	return &newUser
}


func NewAPIUser(product *User) *models.User {
	newUser := models.User{
		ID:         product.ID,
		Name:       &product.Name,
	}
	return &newUser
}

func NewDBBusiness(product *models.Business) *Business {
	newBusiness := Business{
		Model: gorm.Model {
			ID:                product.ID,
		},
		Name:              *product.Name,
	}
	return &newBusiness
}


func NewAPIBusiness(product *Business) *models.Business {
	newBusiness := models.Business{
		ID:         product.ID,
		Name:       &product.Name,
	}
	return &newBusiness
}