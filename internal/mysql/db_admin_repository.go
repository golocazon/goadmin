package mysql

import (
	"errors"
	"github.com/jinzhu/gorm"
	models "gitlab.com/golocazon/goadmin/internal/dbmodels"
)

func NewDBUserRepository(db *gorm.DB) *DBUserRepository {
	return &DBUserRepository{
		DB: db,
	}
}

type DBUserRepository struct {
	DB *gorm.DB
}

func (r *DBUserRepository) Fetch(offset int64, num uint) (Users []*models.User, err error) {
	//Users := make([]dbmodels.User, 0)
	err = r.DB.Limit(num).Offset(offset).Find(&Users).Error
	return
}

func (r *DBUserRepository) GetByID(UserID uint) (error, *models.User) {
	User := &models.User{}
	error := r.DB.First(&User, UserID).Error
	if error != nil {
		return errors.New("not found"), User
	}
	return nil, User
}

func (r *DBUserRepository) Update(User *models.User) (*models.User, error) {
	// Create a new User
	error := r.DB.Save(&User).Error
	return User, error
}

func (r *DBUserRepository) Store(User *models.User) (*models.User, error) {
	// Create a new User
	error := r.DB.Create(&User).Error
	//r.DB.Save(&User)
	return User, error
}

func (r *DBUserRepository) Delete(UserID uint) (bool, error) {
	User := &models.User{}
	User.ID = UserID
	error := r.DB.Delete(&User).Error
	if error != nil {
		return false, errors.New("not found")
	}
	return true, nil
}
