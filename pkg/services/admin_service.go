package services

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/golocazon/goadmin/internal/pkg"
	"gitlab.com/golocazon/goadmin/pkg/models"
	"gitlab.com/golocazon/gologger"
	api2 "gitlab.com/golocazon/gowsfwk/api"
)

type AdminService interface {
	pkg.UserController
}

type AdminServiceClient struct {
	Client *api2.Client
}

func NewAdminServiceClient(client *api2.Client) *AdminServiceClient {
	return &AdminServiceClient{
		Client: client,
	}
}

func (c *AdminServiceClient) CreateUser(User models.User) (error, *models.User) {

	req, err := c.Client.NewRequest("POST", "/api/User", User)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var UserResponse UserResponse
	mapstructure.Decode(response.Content, &UserResponse)
	return err, &UserResponse.User
}

func (c *AdminServiceClient) UpdateUserDetails(UserID int64, User models.User) (error, *models.User) {
	req, err := c.Client.NewRequest("PUT", "/api/user/"+string(UserID), User)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var UserResponse UserResponse
	mapstructure.Decode(response.Content, &UserResponse)
	return err, &UserResponse.User
}

func (c *AdminServiceClient) GetUsers(customerID int64) (error, []*models.User) {
	gologger.Infof("Called GetUsers with customer id %v", customerID)
	req, err := c.Client.NewRequest("GET", "/api/user/"+string(customerID)+"/Users", nil)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var UsersResponse UsersResponse
	mapstructure.Decode(response.Content, &UsersResponse)
	return err, UsersResponse.Users
}

func (c *AdminServiceClient) GetUser(UserID int64) (error, *models.User) {
	gologger.Infof("Called GetUser  with User id %v", UserID)
	req, err := c.Client.NewRequest("GET", "/api/User/"+string(UserID), nil)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var UserResponse UserResponse
	mapstructure.Decode(response.Content, &UserResponse)
	return err, &UserResponse.User
}

func (c *AdminServiceClient) DeleteUser(UserID int64) error {
	gologger.Infof("Delete GetUsers with User id %v", UserID)
	req, err := c.Client.NewRequest("DELETE", "/api/User/"+string(UserID), nil)
	if err != nil {
		return err
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	return err
}

type UsersResponse struct {
	api2.Response
	Users []*models.User `json:"Users"`
}

type UserResponse struct {
	api2.Response
	User models.User `json:"User"`
}
