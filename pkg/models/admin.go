package models

type User struct {
	ID         uint      `json:"id"`
	Name       *string    `json:"name"`
}

type Business struct {
	ID         uint      `json:"id"`
	Name       *string    `json:"name"`
}

func NewUser(name string) *User {
	return &User{Name: &name, }
}

func NewBusiness(name string) *Business {
	return &Business{Name: &name, }
}

