package api

import (
	"encoding/json"
	"testing"
)

func TestUserUnmarshal(t *testing.T) {
	input := "[{\"id\":2,\"name\":\"Test\"},{\"id\":3,\"name\":\"TestPatch\"}]"

	result := &[]User{}
	b := []byte(input)
	if err := json.Unmarshal(b, result); err != nil {
		if (err != nil) {
			t.Errorf("Unexpected error, got %v", err)
		}
		if (len(*result) != 2) {
			t.Errorf("Bad number of users, expecting %d, got %d", len(*result), 2)
		}
		if ((*((*result)[1]).Id) != 3) {
			t.Errorf("Bad id for user, expecting %v, got %v", (*((*result)[1]).Id), "3")
		}
	}
}
